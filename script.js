function addListener(button, mass, numberGroup, tab) {
    for (let i = 0; i < button.length; i++) {
        button[i].addEventListener("click", function () {
            openCity(event, mass[i], numberGroup, tab);
        });
    }
}

function openCity(evt, cityName, numberGroup, tab) {

    let tabContent = numberGroup.getElementsByClassName("tab-content");
    for (let i = 0; i < tabContent.length; i++) {
        tabContent[i].classList.add("close-city");
        tabContent[i].classList.remove("open-city");
    }
    //let tabNumber = document.getElementById("tab");
    let tabLinks = tab.getElementsByClassName("tab-links");
    for (let i = 0; i < tabLinks.length; i++) {
        tabLinks[i].classList.remove("active");
    }
    document.getElementById(cityName).classList.remove("close-city");
    document.getElementById(cityName).classList.add("open-city");
    evt.currentTarget.classList.add("active");
}

function newId() {
    let min = Math.min(0);
    let max = Math.max(654);
    let id = Math.floor(Math.random() * (max - min + 1)) + min;
    return id.toString();
}

function newTab(elem, tabGroup, tabElem, nameTab, contentTab, add) {
    elem.addEventListener("click", function () {
        let id = newId();
        if (nameTab.value.length >= 1 && contentTab.value.length >= 1) {
            let newButton = document.createElement("button");
            newButton.classList.add("tab-links");
            let newDiv = document.createElement("div");
            newDiv.classList.add("tab-content");
            newDiv.id = id;
            let newH = document.createElement("h2");
            newH.textContent = nameTab.value;
            let newP = document.createElement("p");
            newP.textContent = contentTab.value;
            tabGroup.appendChild(newDiv);
            newDiv.appendChild(newH);
            newDiv.appendChild(newP);
            newButton.addEventListener("click", function () {
                openCity(event, id, tabGroup, tabElem);
            });
            newButton.textContent = nameTab.value;
            tabElem.insertBefore(newButton, add);
            nameTab.value = "";
            contentTab.value = "";
        } else
            alert("Заповніть пусті поля");
    })
}

function oneTab() {
    let elem = document.getElementById("new-tab");
    let tabButton = document.getElementById("tab");
    let buttons = tabButton.getElementsByTagName("button");
    let arr = ["kiev", "lviv", "zhytomyr", "create"];
    let numberGroup = document.getElementById("tab-group-one");
    addListener(buttons, arr, numberGroup, tabButton);
    let nameTab = document.getElementById("name-tab");
    let contentTab = document.getElementById("content-tab");
    let add = document.getElementById("add");
    newTab(elem, numberGroup, tabButton, nameTab, contentTab, add);

}

oneTab();

function twoTab() {
    let tabButton = document.getElementById("tab-two");
    let button = tabButton.getElementsByTagName("button");
    let arr = ["life", "money", "danger", "create-two"];
    let numberGroup = document.getElementById("tab-group-two");
    addListener(button, arr, numberGroup, tabButton);
    let elem = document.getElementById("add-new");
    let nameTab = document.getElementById("name-new");
    let contentTab = document.getElementById("content-new");
    let add = document.getElementById("new");
    newTab(elem, numberGroup, tabButton, nameTab, contentTab, add);
}

twoTab();



